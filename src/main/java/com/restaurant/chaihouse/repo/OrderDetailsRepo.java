package com.restaurant.chaihouse.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.restaurant.chaihouse.entity.OrderDetails;



public interface OrderDetailsRepo extends JpaRepository<OrderDetails, Integer> {

}
