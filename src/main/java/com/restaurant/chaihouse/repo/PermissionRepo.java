package com.restaurant.chaihouse.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.restaurant.chaihouse.entity.Permission;



public interface PermissionRepo extends JpaRepository<Permission, Integer> {

}
