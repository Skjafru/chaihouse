package com.restaurant.chaihouse.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.restaurant.chaihouse.entity.Orders;

public interface OrdersRepo extends JpaRepository<Orders, Integer> {

}
