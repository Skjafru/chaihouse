package com.restaurant.chaihouse.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.restaurant.chaihouse.entity.Items;


public interface ItemsRepo extends JpaRepository<Items, Integer> {

}
