package com.restaurant.chaihouse.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.restaurant.chaihouse.entity.Restaurant;



public interface RestaurentRepo extends JpaRepository<Restaurant, Integer> {

}
