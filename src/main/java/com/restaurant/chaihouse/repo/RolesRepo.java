package com.restaurant.chaihouse.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.restaurant.chaihouse.entity.Roles;



public interface RolesRepo extends JpaRepository<Roles, Integer>{

}
