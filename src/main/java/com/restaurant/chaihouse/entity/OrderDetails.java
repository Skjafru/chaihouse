package com.restaurant.chaihouse.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class OrderDetails {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@ManyToOne
	private Orders orders;
	private int itemId;
	private Date orderTime;
	  
	
	public Date getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(Date orderTime) {
		this.orderTime = orderTime;
	}

	public Orders getOrders() {
		return orders;
	}

	public void setOrders(Orders orders) {
		this.orders = orders;
	}

	private int quantity;
	private int totalPrice;
	
	

    public OrderDetails() {
    }
	
	public int getOrderDetaisId() {
		return id;
	}
	public void setOrderDetaisId(int orderDetaisId) {
		this.id = orderDetaisId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		id = id;
	}


	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		itemId = itemId;
	}

	
	
	
	
	

}
