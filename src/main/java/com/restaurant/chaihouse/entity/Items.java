package com.restaurant.chaihouse.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Items {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Id;
	
	private String type;
	private String description;
	private String name;
	private int stock;
	private int price;
	private int restaurentId;
	private String imageUrl;
	@OneToOne
	private Orders orders;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getRestaurentId() {
		return restaurentId;
	}
	public void setRestaurentId(int restaurentId) {
		this.restaurentId = restaurentId;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public Orders getOrders() {
		return orders;
	}
	public void setOrders(Orders orders) {
		this.orders = orders;
	}
	public Items(int id, String type, String description, String name, int stock, int price, int restaurentId,
			String imageUrl, Orders orders) {
		super();
		Id = id;
		this.type = type;
		this.description = description;
		this.name = name;
		this.stock = stock;
		this.price = price;
		this.restaurentId = restaurentId;
		this.imageUrl = imageUrl;
		this.orders = orders;
	}
    
	
	
	
	
	
	
	
	
	
	
	

}
