

package com.restaurant.chaihouse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import springfox.documentation.swagger2.annotations.EnableSwagger2;




@SpringBootApplication
@EnableSwagger2
@EnableDiscoveryClient
@EnableFeignClients
public class ChaihouseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChaihouseApplication.class, args);
	}
	
	
	@LoadBalanced
	  @Bean
	  RestTemplate restTemplate() {
	      return new RestTemplate();
	  }

}
